--[[
     Licensed under GNU General Public License v2
      * (c) 2020, Gerry Agbobada
--]]

local os       = os
local string   = string
local tonumber = tonumber

local weather_codes = {
    ["113"] = "☀️",
    ["116"] = "⛅️",
    ["119"] = "☁️",
    ["122"] = "☁️",
    ["143"] = "🌫",
    ["176"] = "🌦",
    ["179"] = "🌧",
    ["182"] = "🌧",
    ["185"] = "🌧",
    ["200"] = "⛈",
    ["227"] = "🌨",
    ["230"] = "❄️",
    ["248"] = "🌫",
    ["260"] = "🌫",
    ["263"] = "🌦",
    ["266"] = "🌦",
    ["281"] = "🌧",
    ["284"] = "🌧",
    ["293"] = "🌦",
    ["296"] = "🌦",
    ["299"] = "🌧",
    ["302"] = "🌧",
    ["305"] = "🌧",
    ["308"] = "🌧",
    ["311"] = "🌧",
    ["314"] = "🌧",
    ["317"] = "🌧",
    ["320"] = "🌨",
    ["323"] = "🌨",
    ["326"] = "🌨",
    ["329"] = "❄️",
    ["332"] = "❄️",
    ["335"] = "❄️",
    ["338"] = "❄️",
    ["350"] = "🌧",
    ["353"] = "🌦",
    ["356"] = "🌧",
    ["359"] = "🌧",
    ["362"] = "🌧",
    ["365"] = "🌧",
    ["368"] = "🌨",
    ["371"] = "❄️",
    ["374"] = "🌧",
    ["377"] = "🌧",
    ["386"] = "⛈",
    ["389"] = "🌩",
    ["392"] = "⛈",
    ["395"] = "❄️",
}

-- Wttr
-- current weather and X-days forecast
-- gagbo_widgets.wttr

-- Expects the full json from wttr_call
local function format_weather_text(data)
    return string.format([[ %s %d°
]],
weather_codes[data["current_condition"][1]["weatherCode"]],
data["current_condition"][1]["FeelsLikeC"]
)
end

-- Expects the full json from wttr_call
local function format_weather_tooltip_header(data)
    return string.format([[
<b>%s %s°</b>
Ressenti : %s°
Vent : %s km/h
Humidité : %s%%]],
data["current_condition"][1]["lang_fr"][1]["value"],
data["current_condition"][1]["temp_C"],
data["current_condition"][1]["FeelsLikeC"],
data["current_condition"][1]["windspeedKmph"],
data["current_condition"][1]["humidity"]
)
end

-- Expects the json['weather'][day_offset] from wttr_call
local function format_weather_tooltip_forecast(day_offset, data)
    local function format_chances(hour_data)
        local chances = {
            ["chanceoffog"]= "Brouillard",
            ["chanceoffrost"]= "Gel",
            ["chanceofovercast"]= "Nuageux",
            ["chanceofrain"]= "Pluie",
            ["chanceofsnow"]= "Neige",
            ["chanceofsunshine"]= "Dégagé",
            ["chanceofthunder"]= "Orages",
         ["chanceofwindy"] = "Rafales"
        }
        local conditions = ""
        for event in pairs(chances) do
            if tonumber(hour_data[event]) > 0 then
                conditions = conditions .. string.format("%s %s%%, ", chances[event], hour_data[event])
            end
        end
        return conditions:gsub(",%s+$", "")
    end

    local retval = "\n<b>"
    if day_offset == 0 then
        retval = retval .. "Aujourd'hui, "
    elseif day_offset == 1 then
        retval = retval .. "Demain, "
    end
    retval = retval .. data["date"] .. "</b>\n"
    retval = retval .. string.format("⬆️ %d° ⬇️ %d° 🌅 %s 🌇 %s\n",
                                     data["maxtempC"],
                                     data["mintempC"],
                                     data["astronomy"][1]["sunrise"],
                                     data["astronomy"][1]["sunset"])

    for _, hour_data in ipairs(data["hourly"]) do
        if day_offset ~= 0 or
            (tonumber(hour_data["time"]) / 100 >= (os.date("*t", os.time())["hour"] - 2)) then
            retval = retval .. string.format("%04d %s %s° %s, %s\n",
                                             tonumber(hour_data["time"]),
                                             weather_codes[hour_data["weatherCode"]],
                                             hour_data["FeelsLikeC"],
                                             hour_data["lang_fr"][1]["value"],
                                             format_chances(hour_data))
        end
    end

    return retval
end

return {
  ["format_weather_tooltip_forecast"] = format_weather_tooltip_forecast,
  ["format_weather_text"] = format_weather_text,
  ["format_weather_tooltip_header"] = format_weather_tooltip_header,
  ["weather_codes"] = weather_codes
}

--[[
     Licensed under GNU General Public License v2
      * (c) 2020, Gerry Agbobada
--]]

local awful    = require("awful")
local helpers  = require("lain.helpers")
local json     = require("lain.util").dkjson
local focused  = require("awful.screen").focused
local naughty  = require("naughty")
local wibox    = require("wibox")
local wttr_lib = require("gagbo_widgets.wttr_helpers")
local math     = math
local os       = os
local string   = string
local type     = type
local tonumber = tonumber


local function factory(args)
    local args                  = args or {}
    local wttr_call             = args.wttr_call  or ("curl -s https://wttr.in/?format=j1&lang=fr")
    local weather               = { widget = args.widget or wibox.widget.textbox() }
    local timeout               = args.timeout or (60 * 15) -- 15 min
    local icons_path            = args.icons_path or helpers.icons_dir .. "openweathermap/"
    local notification_preset   = args.notification_preset or {}
    local notification_text_fun = args.notification_text_fun or
                                  function (wn)
                                      local retval = wttr_lib.format_weather_tooltip_header(wn)
                                      for i, day_data in ipairs(wn["weather"]) do
                                          retval = retval .. wttr_lib.format_weather_tooltip_forecast(i, day_data)
                                      end
                                      return retval
                                  end
    local weather_na_markup     = args.weather_na_markup or " NoWeather "
    local followtag             = args.followtag or false
    local showpopup             = args.showpopup or "on"

    weather.widget:set_markup(weather_na_markup)
    weather.icon_path = icons_path .. "na.png"
    weather.icon = wibox.widget.imagebox(weather.icon_path)

    function weather.show(seconds)
        weather.hide()

        if followtag then
            notification_preset.screen = focused()
        end

        if not weather.notification_text then
            weather.update()
        end

        weather.notification = naughty.notify {
            preset  = notification_preset,
            text    = weather.notification_text,
            icon    = weather.icon_path,
            timeout = type(seconds == "number") and seconds or notification_preset.timeout
        }
    end

    function weather.hide()
        if weather.notification then
            naughty.destroy(weather.notification)
            weather.notification = nil
        end
    end

    function weather.attach(obj)
        obj:connect_signal("mouse::enter", function()
            weather.update()
            weather.show(0)
        end)
        obj:connect_signal("mouse::leave", function()
            weather.hide()
        end)
    end

    function weather.update()
        local cmd = wttr_call
        awful.spawn.easy_async(
            cmd,
            function (stdout, stderr, _, exit_code)
                if exit_code ~= 0 then
                    weather.widget:set_markup(weather_na_markup)
                    weather.notification_text = string.format("Error: %s", stderr)
                    do return end
                end

                local weather_wttr, _, err = json.decode(stdout, 1, nil)
                weather.notification_text = string.format("Error: %s", tostring(err))
                if not err and type(weather_wttr) == "table" then
                    weather.notification_text = notification_text_fun(weather_wttr)
                    weather.icon_path = icons_path .. "na.png"
                    weather.widget:set_markup(wttr_lib.format_weather_text(weather_wttr))
                else
                    weather.notification_text = err
                    weather.icon_path = icons_path .. "na.png"
                    weather.widget:set_markup(weather_na_markup)
                end
                weather.icon:set_image(weather.icon_path)
        end)
    end

    if showpopup == "on" then weather.attach(weather.widget) end

    weather.timer = helpers.newtimer("weather-wttr", timeout, weather.update, false, true)

    return weather
end

return factory

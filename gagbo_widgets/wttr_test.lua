local wttr_lib = require("gagbo_widgets.wttr_helpers")
local open = io.open
local json = dofile("lain/util/dkjson.lua")

local function read_file(path)
    local file = open(path, "r")
    if not file then return nil end
    local content = file:read "*a"
    file:close()
    return content
end

local test_file = "gagbo_widgets/test_weather.json"
local test_content = read_file(test_file)

local function notif_function(wn)
    local retval = wttr_lib.format_weather_tooltip_header(wn)
    for i, day_data in ipairs(wn["weather"]) do
        retval = retval .. wttr_lib.format_weather_tooltip_forecast(i, day_data)
    end
    return retval
end

local weather_wttr, _, err = json.decode(test_content, 1, nil)
print(string.format("Error: %s", err))


local bar_text = wttr_lib.format_weather_text(weather_wttr)
print(string.format("Notif Header: %s", tostring(bar_text)))

local notification_text = notif_function(weather_wttr)
print(string.format("Notif body: %s", tostring(notification_text)))

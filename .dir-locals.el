;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((lsp-file-watch-threshold . 600)))
 (lua-mode . ((lsp-lua-diagnostics-globals . ["awesome"])
              (lsp-lua-diagnostics-disable . ["undefined-global"] ))))

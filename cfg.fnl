;; luacheck: globals screen client awesome

;; Standard awesome library
(local awful (require :awful))
(require :awful.autofocus)
;; Theme handling
(local beautiful (require :beautiful))
;; Enable hotkeys help widget for VIM and other apps
;; when client with a matching name is opened:
(require :awful.hotkeys_popup.keys)

(local titlebars (require :titlebars))

;; * Initialize
;; Themes define colours, icons, font and wallpapers.
;; (awful.spawn.with_shell "xrdb -merge ~/.Xresources")
(beautiful.init (.. (awful.util.getdir :config) :themes/gorgehousse/theme.lua))

(local config
       (let [(config-available? config-or-msg) (pcall require :config)]
         (if config-available?
             config-or-msg
             (require :config_default))))

(let [keys (require config.keys)]
  ;; luacheck: globals root
  (root.keys keys.globalkeys)
  (root.buttons keys.desktopbuttons))

(client.connect_signal
 "request::titlebars"
 (fn [c]
  (let [buttons titlebars.buttons
        titlebar (awful.titlebar c)
        wibox (require :wibox)]
    (titlebar:setup {1                  ; Left
                     {1 (awful.titlebar.widget.iconwidget c)
                      : buttons
                      :layout wibox.layout.fixed.horizontal}
                     2                  ; Middle
                     {1                 ; Title
                      {:align :center :widget (awful.titlebar.widget.titlewidget c)}
                      : buttons
                      :layout wibox.layout.flex.horizontal}
                     3                  ; Right
                     {1 (awful.titlebar.widget.floatingbutton c)
                      2 (awful.titlebar.widget.maximizedbutton c)
                      3 (awful.titlebar.widget.stickybutton c)
                      4 (awful.titlebar.widget.ontopbutton c)
                      5 (awful.titlebar.widget.closebutton c)
                      :layout (wibox.layout.fixed.horizontal)}

                     :layout wibox.layout.align.horizontal}))))

;; Bar configuration
(let [bar (require config.bar)]
  (bar.setup))

;; * {{{ Error handling
;; Check if awesome encountered an error during startup and fell back to
;; _nother config (This code will only ever execute for the fallback config)
(when awesome.startup_errors
  (let [naughty (require :naughty)]
    (naughty.notify {:preset naughty.config.presets.critical
                     :title "Oops, there were errors during startup!"
                     :text awesome.startup_errors})))

;; Handle runtime errors after startup
(do
  (var in-error false)
  (awesome.connect_signal "debug::error"
                          (fn [err]
                            (when in-error
                              (lua "return "))
                            (set in-error true)
                            (let [naughty (require :naughty)]
                              (naughty.notify {:preset naughty.config.presets.critical
                                               :title "Oops, an error happened!"
                                               :text (tostring err)}))
                            (set in-error false))))

(set awful.layout.layouts
     [awful.layout.suit.floating
      awful.layout.suit.tile
      awful.layout.suit.tile.left
      awful.layout.suit.tile.bottom
      awful.layout.suit.tile.top
      awful.layout.suit.fair
      awful.layout.suit.fair.horizontal
      awful.layout.suit.spiral
      awful.layout.suit.spiral.dwindle
      awful.layout.suit.max
      awful.layout.suit.max.fullscreen
      awful.layout.suit.magnifier
      awful.layout.suit.corner.nw])

;; * {{{ Menu
;; ** Create a launcher widget and a main menu
;; ** Menubar configuration
;; local app_folders = config.app_folders
(let [menubar (require :menubar)]
  (set menubar.utils.terminal config.terminal))

;; Wallpaper
(lambda set-wallpaper [s]
  (let [gears (require :gears)]
    (when beautiful.wallpaper
      (var wallpaper beautiful.wallpaper)
      (when (= (type wallpaper) :function)
        (set wallpaper (wallpaper s)))
      (gears.wallpaper.maximized wallpaper s true))))

;; Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
(screen.connect_signal "property::geometry"
                       set-wallpaper)


(awful.screen.connect_for_each_screen
 (fn [s]
   (do
     (set-wallpaper s)
     (local names
            [:main :www :term :read :office :irc :7 :8 :9])
     (local l awful.layout.suit)
     (local layouts
            [l.tile l.tile l.tile l.tile l.tile l.tile l.tile l.tile l.floating])
     (awful.tag names s layouts))))

(let [keys (require config.keys)]
  (set awful.rules.rules
       [
        ;; All clients match this rule
        {:properties {:border_color beautiful.border_normal
                      :focus awful.client.focus.filter
                      :keys keys.clientkeys
                      :border_width beautiful.border_width
                      :raise true
                      :buttons keys.clientbuttons
                      :screen awful.screen.preferred
                      :placement (+ awful.placement.no_overlap
                                    awful.placement.no_offscreen)}
         :rule {}}
        ;; Floating clients
        {:properties {:floating true}
         :rule_any {:instance [:DTA :copyq :pinentry]
                    :class [:Arandr :Blueman-manager :Gpick
                            :Kruler
                            ;; Kalarm
                            :MessageWin
                            :KeePassXC :Nextcloud :Sxiv
                            ;; Needs a fixed window size to avoid fingerprinting
                            "Tor Browser"
                            :Wpa_gui :veromix :xtightvncviewer
                            :fst]
                    :name ["Event Tester"]
                    :role [:AlarmWindow :ConfigManager :pop-up]}}
        ;; Add titlebars to dialogs
        {:properties {:titlebars_enabled true}
         :rule_any {:type [:dialog]}}
        ;; Remove titlebars from normal windows (clients)
        {:properties {:titlebars_enabled false}
         :rule_any {:type [:normal]}}]))

;; * {{{ Signals
;; ** Signal function to execute when a new client appears.
(client.connect_signal
 :manage
 (fn [c]
   (when (not awesome.startup)
     (awful.client.setslave c))
   (when (and (and awesome.startup
                   (not c.size_hints.user_position))
              (not c.size_hints.program_position))
     (awful.placement.no_offscreen c))))

(client.connect_signal
 "mouse::enter"
 (fn [c]
   (c:emit_signal "request::activate" :mouse_enter
                  {:raise false})))

(client.connect_signal
 :focus
 (fn [c]
   (set c.border_color beautiful.border_focus)))

(client.connect_signal
 :unfocus
 (fn [c]
   (set c.border_color beautiful.border_normal)))

;; * {{{ Autostart
(awful.spawn.with_shell config.shell_autostart)

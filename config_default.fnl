(local awful (require :awful))
(local hotkeys-popup (require :awful.hotkeys_popup))
(local beautiful (require :beautiful))

(local config {})

(set config.modkey :Mod4)

(set config.terminal :alacritty)
(set config.floating_terminal (.. config.terminal " -c fst "))

(set config.editor (or (os.getenv :EDITOR) :nvim))
(set config.editor_cmd (.. config.terminal " -e " config.editor))
(set config.graphical_emacs "emacsclient -n -c -a ''")

(set config.screen_lock "xscreensaver-command -lock")

(set config.launcher "rofi -combi-modi 'window,drun' -modi 'combi' -show combi")
(set config.info :htop)
(set config.info_cmd (.. config.floating_terminal " -e " config.info))
(set config.music_player :ncmpcpp)
(set config.music_player_cmd
     (.. config.floating_terminal " -e " config.music_player))
(set config.browser :firefox)

(set config.myawesomemenu
     [[:hotkeys
       (fn []
         (hotkeys-popup.show_help nil (awful.screen.focused)))]
      [:manual (.. config.terminal " -e man awesome")]
      ["edit config" (.. config.editor_cmd " " awesome.conffile)]
      [:restart awesome.restart]
      [:quit
       (fn []
         (awesome.quit))]])

(set config.mymainmenu
     (awful.menu {:items [[:awesome
                           config.myawesomemenu
                           beautiful.awesome_icon]
                          ["open terminal" config.terminal]
                          [:Emacs config.graphical_emacs]]}))

(set config.mylauncher
     (awful.widget.launcher {:image beautiful.awesome_icon
                             :menu config.mymainmenu}))
(set config.app_folders
     [:/usr/share/applications/
      :/usr/local/share/applications/
      "~/.local/share/applications/"])
(set config.net_widgets_wireless
     {:interface (or (os.getenv :WIRELESS_NAME) :wlp4s0)})

(set config.net_widgets_wired [(or (os.getenv :ETH_NAME) :enp0s31f6)])
(set config.weather_widget_city "paris,fr")
(set config.weather_widget_units :metric)
(set config.bar :bar_themes.kadabra)
(set config.keys :keys)
(set config.shell_autostart "~/.config/awesome/autostart.sh")

config

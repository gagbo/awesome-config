(pcall require :luarocks.loader)

(local fennel (require :./fennel))

(set fennel.path (.. fennel.path ";.config/awesome/?.fnl"))

(table.insert (or package.loaders package.searchers) fennel.searcher)

(local gears (require :gears))

(local awful (require :awful))

(local wibox (require :wibox))

(require :awful.autofocus)

(local beautiful (require :beautiful))

(local naughty (require :naughty))

(local menubar (require :menubar))

(require :awful.hotkeys_popup.keys)

(beautiful.init (.. (awful.util.getdir :config) :themes/gorgehousse/theme.lua))

(var (config-available config) (pcall require :config))

(when (not config-available)
  (set config (require :config_default)))

(let [keys (require config.keys)]
  (root.keys keys.globalkeys)
  (root.buttons keys.desktopbuttons))

(local titlebars (require :titlebars))

(client.connect_signal "request::titlebars"
                       (fn [c]
                         (let [buttons titlebars.buttons]
                           (: (awful.titlebar c) :setup
                              {1 {1 (awful.titlebar.widget.iconwidget c)
                                  : buttons
                                  :layout wibox.layout.fixed.horizontal}
                               2 {1 {:align :center
                                     :widget (awful.titlebar.widget.titlewidget c)}
                                  : buttons
                                  :layout wibox.layout.flex.horizontal}
                               3 {1 (awful.titlebar.widget.floatingbutton c)
                                  2 (awful.titlebar.widget.maximizedbutton c)
                                  3 (awful.titlebar.widget.stickybutton c)
                                  4 (awful.titlebar.widget.ontopbutton c)
                                  5 (awful.titlebar.widget.closebutton c)
                                  :layout (wibox.layout.fixed.horizontal)}
                               :layout wibox.layout.align.horizontal}))))

((. (require config.bar) :setup))

(when awesome.startup_errors
  (naughty.notify {:preset naughty.config.presets.critical
                   :title "Oops, there were errors during startup!"
                   :text awesome.startup_errors}))

(do
  (var in-error false)
  (awesome.connect_signal "debug::error"
                          (fn [err]
                            (when in-error
                              (lua "return "))
                            (set in-error true)
                            (naughty.notify {:preset naughty.config.presets.critical
                                             :title "Oops, an error happened!"
                                             :text (tostring err)})
                            (set in-error false))))

(set awful.layout.layouts
     {1 awful.layout.suit.floating
      2 awful.layout.suit.tile
      3 awful.layout.suit.tile.left
      4 awful.layout.suit.tile.bottom
      5 awful.layout.suit.tile.top
      6 awful.layout.suit.fair
      7 awful.layout.suit.fair.horizontal
      8 awful.layout.suit.spiral
      9 awful.layout.suit.spiral.dwindle
      10 awful.layout.suit.max
      11 awful.layout.suit.max.fullscreen
      12 awful.layout.suit.magnifier
      13 awful.layout.suit.corner.nw})

(set menubar.utils.terminal config.terminal)

(fn set-wallpaper [s]
  (when beautiful.wallpaper
    (var wallpaper beautiful.wallpaper)
    (when (= (type wallpaper) :function)
      (set wallpaper (wallpaper s)))
    (gears.wallpaper.maximized wallpaper s true)))

(screen.connect_signal "property::geometry" set-wallpaper)

(awful.screen.connect_for_each_screen (fn [s]
                                        (do
                                          (set-wallpaper s)
                                          (local names
                                                 {1 :main
                                                  2 :www
                                                  3 :term
                                                  4 :read
                                                  5 :office
                                                  6 :irc
                                                  7 :7
                                                  8 :8
                                                  9 :9})
                                          (local l awful.layout.suit)
                                          (local layouts
                                                 {1 l.tile
                                                  2 l.tile
                                                  3 l.tile
                                                  4 l.tile
                                                  5 l.tile
                                                  6 l.tile
                                                  7 l.tile
                                                  8 l.tile
                                                  9 l.floating})
                                          (awful.tag names s layouts))))

(local keys (require config.keys))

(set awful.rules.rules
     {1 {:properties {:border_color beautiful.border_normal
                      :focus awful.client.focus.filter
                      :keys keys.clientkeys
                      :border_width beautiful.border_width
                      :raise true
                      :buttons keys.clientbuttons
                      :screen awful.screen.preferred
                      :placement (+ awful.placement.no_overlap
                                    awful.placement.no_offscreen)}
         :rule {}}
      2 {:properties {:floating true}
         :rule_any {:instance {1 :DTA 2 :copyq 3 :pinentry}
                    :class {1 :Arandr
                            2 :Blueman-manager
                            3 :Gpick
                            4 :Kruler
                            5 :MessageWin
                            6 :KeePassXC
                            7 :Nextcloud
                            8 :Sxiv
                            9 "Tor Browser"
                            10 :Wpa_gui
                            11 :veromix
                            12 :xtightvncviewer
                            13 :fst}
                    :name {1 "Event Tester"}
                    :role {1 :AlarmWindow 2 :ConfigManager 3 :pop-up}}}
      3 {:properties {:titlebars_enabled true} :rule_any {:type {1 :dialog}}}
      4 {:properties {:titlebars_enabled false} :rule_any {:type {1 :normal}}}})

(client.connect_signal :manage
                       (fn [c]
                         (when (not awesome.startup)
                           (awful.client.setslave c))
                         (when (and (and awesome.startup
                                         (not c.size_hints.user_position))
                                    (not c.size_hints.program_position))
                           (awful.placement.no_offscreen c))))

(client.connect_signal "mouse::enter"
                       (fn [c]
                         (c:emit_signal "request::activate" :mouse_enter
                                        {:raise false})))

(client.connect_signal :focus
                       (fn [c]
                         (set c.border_color beautiful.border_focus)))

(client.connect_signal :unfocus
                       (fn [c]
                         (set c.border_color beautiful.border_normal)))

(awful.spawn.with_shell config.shell_autostart)


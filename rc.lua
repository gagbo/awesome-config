-- This uses
-- The global awesome object
-- the global screen class : https://awesomewm.org/doc/api/classes/screen.html
-- the global client class : https://awesomewm.org/doc/api/classes/client.html
-- luacheck: globals screen client awesome

-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local fennel = require("./fennel")
fennel.path = fennel.path .. ";.config/awesome/?.fnl"
table.insert(package.loaders or package.searchers, fennel.searcher)

require("cfg")

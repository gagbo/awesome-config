(local awful (require :awful))
(local gears (require :gears))
(local beautiful (require :beautiful))
(local xresources (require :beautiful.xresources))

(local dpi xresources.apply_dpi)
(local wibox (require :wibox))


(lambda rrect [radius]
  (fn [cr width height]
    (gears.shape.rounded_rect cr width height radius)))

(lambda rbar []
  (fn [cr width height]
    (gears.shape.rounded_bar cr width height)))

(lambda prrect [radius tl tr br bl]
  (fn [cr width height]
    (gears.shape.partially_rounded_rect cr width height tl tr br bl radius)))

(lambda infobubble [radius]
  (fn [cr width height]
    (gears.shape.infobubble cr width height radius)))

(lambda rect []
  (fn [cr width height]
    (gears.shape.rectangle cr width height)))

(lambda circle []
  (fn [cr width height]
    (gears.shape.circle cr width height)))

(lambda colorize_text [txt fg]
  (.. "<span foreground='" fg "'>" txt :</span>))

(lambda client_menu_toggle []
  (var instance nil)
  (fn []
    (if (and instance instance.wibox.visible)
        (do
          (instance:hide)
          (set instance nil))
        (set instance (awful.menu.clients {:theme {:width (dpi 250)}})))))

(lambda pad [size]
  (var str "")
  (for [_ 1 size 1]
    (set str (.. str " ")))
  (local pad (wibox.widget.textbox str))
  pad)

(lambda move_to_edge [c direction]
  (let [workarea (. (awful.screen.focused) :workarea)
        client-geometry (c:geometry)]
    (if (= direction :up)
        (c:geometry {1 nil
                     3 nil
                     4 nil
                     :y (+ workarea.y (* beautiful.screen_margin 2))})
        (= direction :down)
        (c:geometry {1 nil
                     3 nil
                     4 nil
                     :y (- (- (- (+ workarea.height workarea.y)
                                 client-geometry.height)
                              (* beautiful.screen_margin 2))
                           (* beautiful.border_width 2))})
        (= direction :left)
        (c:geometry {2 nil
                     3 nil
                     4 nil
                     :x (+ workarea.x (* beautiful.screen_margin 2))})
        (= direction :right)
        (c:geometry {2 nil
                     3 nil
                     4 nil
                     :x (- (- (- (+ workarea.width workarea.x)
                                 client-geometry.width)
                              (* beautiful.screen_margin 2))
                           (* beautiful.border_width 2))}))))

(lambda create_titlebar [c
                             titlebar-buttons
                             titlebar-position
                             titlebar-size]
  (: (awful.titlebar c {:size titlebar-size
                        :font beautiful.titlebar_font
                        :position titlebar-position}) :setup
     {1 {:buttons titlebar-buttons :layout wibox.layout.fixed.horizontal}
      2 {:buttons titlebar-buttons :layout wibox.layout.fixed.horizontal}
      3 {:buttons titlebar-buttons :layout wibox.layout.fixed.horizontal}
      :layout wibox.layout.align.horizontal}))

(var double-tap-timer nil)
  (lambda single_double_tap [single-tap-function double-tap-function]
    (if double-tap-timer
        (do
          (double-tap-timer:stop)
          (set double-tap-timer nil)
          (double-tap-function))
        (set double-tap-timer
             (gears.timer.start_new 0.2 (fn []
                                          (set double-tap-timer nil)
                                          (single-tap-function)
                                          false)))))

(lambda toggle_scratchpad []
  (let [screen (awful.screen.focused)]
    (if (and (not= client.focus nil) (= client.focus.class :scratchpad))
        (do
          (local tag (. screen.tags 10))
          (when tag
            (client.focus:move_to_tag tag)))
        (do
          (local current-tag screen.selected_tag)
          (fn scratchpad-client [c]
            (awful.rules.match c {:class :scratchpad}))
          (each [c (awful.client.iterate scratchpad-client)]
            (set c.minimized false)
            (c:move_to_tag current-tag)
            (set client.focus c)
            (c:raise))))))

(lambda add_clickable_effect [w]
  (let [original-cursor :left_ptr
        hover-cursor :hand1]
    (w:connect_signal "mouse::enter"
                      (fn []
                        (let [wib _G.mouse.current_wibox]
                          (when wib
                            (set wib.cursor hover-cursor)))))
    (w:connect_signal "mouse::leave"
                      (fn []
                        (let [wib _G.mouse.current_wibox]
                          (when wib
                            (set wib.cursor original-cursor)))))))

(lambda getHostname []
  (let [f (io.popen :/bin/hostname)]
    (var hostname (or (f:read :*a) ""))
    (f:close)
    (set hostname (string.gsub hostname "\n$" ""))
    hostname))

{: getHostname : rrect : rbar
 : prrect : infobubble : rect
 : circle : colorize_text
 : client_menu_toggle : pad
 : move_to_edge : create_titlebar
 : single_double_tap : toggle_scratchpad
 : add_clickable_effect}

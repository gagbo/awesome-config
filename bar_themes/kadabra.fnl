(local gears (require :gears))
(local awful (require :awful))
(local wibox (require :wibox))
(local beautiful (require :beautiful))
(local lain (require :lain))

(local widget-font (or beautiful.font "KoHo 12"))

(local weather-widget (require :gagbo_widgets.wttr))

(local brightness-widget
       (require :awesome-wm-widgets.brightness-widget.brightness))

(local cpu-widget (require :awesome-wm-widgets.cpu-widget.cpu-widget))
(local volumebar-widget
       (require :awesome-wm-widgets.volumebar-widget.volumebar))
(local ram-widget (require :awesome-wm-widgets.ram-widget.ram-widget))
(local batteryarc-widget
       (require :awesome-wm-widgets.batteryarc-widget.batteryarc))
(local net-widgets (require :net_widgets))
(local config (require :../config))

(local secrets
       (let [(secrets-available secrets-or-msg) (pcall require :secrets)]
         (if secrets-available
             secrets-or-msg
             (require :secrets-example))))

(local bar-theme-path "~/.config/awesome/bar_themes/")

(lambda getcputempfile []
  (let [file (io.popen "bash -c 'echo  /sys/devices/platform/coretemp.0/hwmon/hwmon*/temp1_input'")
        stdout (file:read :*l)]
    stdout))

(local tempfile (getcputempfile))

(local temp-widget
       (let [lain-widget (lain.widget.temp
                          {:timeout 10
                           : tempfile
                           :settings (fn []
                                       (let [widget _G.widget
                                             coretemp-now _G.coretemp-now]
                                           (widget:set_markup (.. " CPU : " coretemp-now "C "))))})]
         lain-widget.widget))

(local mysysload
       (let [lain-widget (lain.widget.sysload
                          {:settings (fn []
                                       (let [widget _G.widget
                                             load_1 _G.load_1
                                             load_5 _G.load_5
                                             load_15 _G.load_15]
                                         (widget:set_markup (.. load_1 " " load_5 " " load_15 " "))))})]
         lain-widget.widget))

(local net-wireless (net-widgets.wireless config.net_widgets_wireless))

(local net-wired (net-widgets.indicator {:interfaces config.net_widgets_wired
                                         :timeout 5}))

(local mybattery (batteryarc-widget {:thickness 3
                                     :charging_color beautiful.widget_green
                                     :show_current_level true
                                     :font widget-font}))

(local mytextclock (wibox.widget.textclock "%a %d %b %H:%M:%S" 1))

(local myweatherwidget (weather-widget {}))

(local mybrightnesswidget
       (brightness-widget {:inc_brightness_cmd "xbacklight -inc 4"
                           :get_brightness_cmd "xbacklight -get"
                           :dec_brightness_cmd "xbacklight -dec 4"
                           :font widget-font
                           :path_to_icon (.. bar-theme-path
                                             :display-brightness.svg)}))

(local myvolumebarwidget (volumebar-widget))

(local mycpuwidget (cpu-widget))

(local myramwidget (ram-widget))

(local taglist-buttons
       (gears.table.join (awful.button {} 1
                                       (fn [t]
                                         (t:view_only)))
                         (awful.button {1 config.modkey} 1
                                       (fn [t]
                                         (when client.focus
                                           (client.focus:move_to_tag t))))
                         (awful.button {} 3 awful.tag.viewtoggle)
                         (awful.button {1 config.modkey} 3
                                       (fn [t]
                                         (when client.focus
                                           (client.focus:toggle_tag t))))
                         (awful.button {} 4
                                       (fn [t]
                                         (awful.tag.viewnext t.screen)))
                         (awful.button {} 5
                                       (fn [t]
                                         (awful.tag.viewprev t.screen)))))

(local tasklist-buttons
       (gears.table.join (awful.button {} 1
                                       (fn [c]
                                         (if (= c client.focus)
                                             (set c.minimized true)
                                             (c:emit_signal "request::activate"
                                                            :tasklist
                                                            {:raise true}))))
                         (awful.button {} 3
                                       (fn []
                                         (awful.menu.client_list {:theme {:width 250}})))
                         (awful.button {} 4
                                       (fn []
                                         (awful.client.focus.byidx 1)))
                         (awful.button {} 5
                                       (fn []
                                         (awful.client.focus.byidx (- 1))))))

(local mykeyboardlayout (awful.widget.keyboardlayout))

(lambda bar-setup []
  (awful.screen.connect_for_each_screen
   (lambda [s]
     (set s.mypromptbox (awful.widget.prompt))
     (set s.mylayoutbox (awful.widget.layoutbox s))
     (s.mylayoutbox:buttons
      (gears.table.join (awful.button {} 1
                                      (fn []
                                        (awful.layout.inc 1)))
                        (awful.button {} 3
                                      (fn []
                                        (awful.layout.inc (- 1))))
                        (awful.button {} 4
                                      (fn []
                                        (awful.layout.inc 1)))
                        (awful.button {} 5
                                      (fn []
                                        (awful.layout.inc (- 1))))))
     (set s.mytaglist
          (awful.widget.taglist
           {:filter awful.widget.taglist.filter.all
            :screen s
            :buttons taglist-buttons}))
     (set s.mytasklist
          (awful.widget.tasklist
           {:filter awful.widget.tasklist.filter.currenttags
            :screen s
            :buttons tasklist-buttons}))
     (set s.mywibox
          (awful.wibar
           {:position :top
            :screen s}))
     ;; Left / Center / Right
     (s.mywibox:setup {1 {2 s.mytaglist
                          3 s.mypromptbox
                          4 mybrightnesswidget
                          :layout wibox.layout.fixed.horizontal
                          5 myvolumebarwidget}
                       2 s.mytasklist
                       3 {2 mytextclock
                          3 myweatherwidget
                          4 net-wired
                          5 net-wireless
                          6 temp-widget
                          7 mysysload
                          8 mycpuwidget
                          9 myramwidget
                          10 mykeyboardlayout
                          11 mybattery
                          12 (wibox.widget.systray)
                          13 s.mylayoutbox
                          :layout wibox.layout.fixed.horizontal}
                       :layout wibox.layout.align.horizontal}))))

{:setup bar-setup}

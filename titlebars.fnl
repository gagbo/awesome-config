(local awful (require :awful))
(local gears (require :gears))

(local buttons
       (let [awful (require :awful)
             gears (require :gears)]
         (gears.table.join (awful.button {} 1
                                         (fn []
                                           (let [c (_G.mouse.object_under_pointer)]
                                             (set client.focus c)
                                             (c:raise)
                                             (awful.mouse.client.move c))))
                           (awful.button {} 2
                                         (fn []
                                           (let [window-to-kill (_G.mouse.object_under_pointer)]
                                             (window-to-kill:kill))))
                           (awful.button {} 3
                                         (fn []
                                           (let [c (_G.mouse.object_under_pointer)]
                                             (set client.focus c)
                                             (c:raise)
                                             (awful.mouse.client.resize c))))
                           (awful.button {} 9
                                         (fn []
                                           (let [c (_G.mouse.object_under_pointer)]
                                             (set client.focus c)
                                             (c:raise)
                                             (set c.floating (not c.floating)))))
                           (awful.button {} 8
                                         (fn []
                                           (let [c (_G.mouse.object_under_pointer)]
                                             (set client.focus c)
                                             (c:raise)
                                             (set c.ontop (not c.ontop))))))))

{: buttons}
